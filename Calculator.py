from sympy import diff


class GradientCalculator:

    def __init__(self, function):
        self.derivatives = {}
        self.compute_derivatives(function)

    def compute_derivatives(self, function):
        for var in function.free_symbols:
            self.derivatives[var] = diff(function, var)

    # point -> a list in the form [[x, x_value], [y, y_value], [z, z_value], ...]
    def compute(self, point):
        gradient = []
        for var, value in point:
            result = self.derivatives[var].subs(point)
            gradient.append([var, result])
        return gradient
