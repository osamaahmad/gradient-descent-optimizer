from Optimizer import GradientDescentOptimizer
from Functions import SumOfSquares
from random import random
from sympy import symbols


def get_random_point(variables, min_val=-100, max_val=100):
    return [[variables[i], min_val + random() * (max_val - min_val)] for i in range(len(variables))]


def run_example(expression, scalar=1e-3, iterations=5000, min_val=-100, max_val=100):
    print('Expression: ', expression)
    point = get_random_point(list(expression.free_symbols), min_val, max_val)
    optimizer = GradientDescentOptimizer(expression, point)
    optimizer.optimize(scalar, iterations)
    print('\tResult: ', optimizer.get_point())


def example_1():
    x = symbols('x')
    expression = x ** 2 + 5
    run_example(expression, 1e-1, 1000)


def example_2():
    x, y, z = symbols('x y z')
    expression = x**2 + y**2 - 14
    run_example(expression, 1e-1)


def example_3():
    # Linear Regression
    expression = SumOfSquares([(1, 2), (2, 5), (3, 7), (4, 4), (5, 3), (6, 6)])
    run_example(expression)


example_1()
example_2()
example_3()
