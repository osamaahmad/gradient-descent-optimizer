from sympy import symbols


# Used to compute linear regression.
# point -> a list in the form [(x1, y1), (x2, y2), ...]
def SumOfSquares(points):
    m, b = symbols('m b')
    result = 0
    for x, y in points:
        result += (m * x + b - y)**2
    return result
