from Calculator import GradientCalculator


class GradientDescentOptimizer:

    def __init__(self, function, initial_point):
        self.gradient_calculator = GradientCalculator(function)
        self.point = initial_point

    def get_point(self):
        return self.point

    # scalar, aka "Learning Rate"
    def optimize(self, scalar=1e-3, iterations=5000):
        for _ in range(iterations):
            gradient = self.gradient_calculator.compute(self.point)
            for i in range(len(gradient)):
                # This assumes that the variables in the gradient
                #  will be in the same order as in the point
                self.point[i][1] -= gradient[i][1] * scalar

        return self.get_point()
